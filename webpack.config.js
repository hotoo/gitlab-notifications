'use strict';

const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const env = process.env.NODE_ENV;

const options = {
  entry: {
    background: './src/background.js',
    contentscript: './src/contentscript.js',
    options_page: './src/options_page.js',
    notifications_page: './src/notifications_page.js',
  },
  module: {
    loaders: [
      { test: /\.less$/, loader: ExtractTextPlugin.extract('css?sourceMap!less?sourceMap') },
      { test: /\.css$/, loader: ExtractTextPlugin.extract('css?sourceMap') },
      { test: /\.(png|jpg)$/, loader: 'url-loader?limit=8192' },
      { test: /\.json$/, loader: 'json-loader' },
      { test: /\.jsx?$/, loader: 'babel', exclude: /node_modules/ },
      { test: /\.atpl$/, loader: 'atpl' },
      { test: /\.woff(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&minetype=application/font-woff' },
      { test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&minetype=application/font-woff' },
      { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&minetype=application/octet-stream' },
      { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file' },
      { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&minetype=image/svg+xml' },
    ],
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.json', '.less', '.css', '.png'],
  },

  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
    chunkFilename: '[name].js',
  },

  plugins: [
    new ExtractTextPlugin('[name].css', {
      disable: false,
      allChunks: true,
    }),
    new CopyPlugin([
      {from: './src/manifest.json'},
      {from: './src/options.html'},
      {from: './src/notifications.html'},
      {from: './test/runner.html'},
      {from: './src/img/', to: 'img/'},
      {from: './src/_locales/', to: '_locales/'},
    ]),
  ],
  externals: {
    chrome: 'chrome',
  },
};

if (env === 'production') {
  options.plugins.push(new webpack.optimize.UglifyJsPlugin({
    compress: { warnings: false },
  }));
} else if (env === 'unittest') {
  options.devtool = 'source-map';
  options.entry.spec = './test/index.test.js';
} else {
  options.devtool = 'source-map';
}

module.exports = options;
