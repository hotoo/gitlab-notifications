# CHANGELOG

## 1.3.4 (2017-02-06)

- Fixed: escape notification message title. https://github.com/hotoo/gitlab-notifications/issues/6

## 1.3.3 (2016-09-05)

- fix: account information(user name, private token) for GitLab v8.14.
  https://github.com/hotoo/gitlab-notifications/issues/5

## 1.3.2 (2016-11-30)

- config: change request gitlab api timeout.

## 1.3.1 (2016-09-05)

- feat(monitor): better monitor gitlab information and failes.
- test(eslint): add eslint for test.

## 1.3.0 (2016-09-05)

- fix: duplicate get events. #22
- feat: monitor gitlab version. #18
- feat: account status. #26
- feat: support get gitlab.com account infomation. #27

## 1.2.6 (2016-09-04)

- feat: options page support gitlab.com
- feat: sign in and retry.
- refact: get user name and private token by gitlab api generator.

## 1.2.5 (2016-08-29)

- feat(options): better add account experience。
- fix(notification): browser notification when use watching notification type.
- fix(monitor): monitor.calc().
- feat(monitor): monitor.time(), monitor.timeEnd().

## 1.2.4 (2016-08-29)

- fix: event.timestamp in unwatching events.
- fix: open target for parojects.

## 1.2.3 (2016-08-28)

- fix: more browser notifications.
- feat(status): issue-reopened, mergerequest-reopened.

## 1.2.2 (2016-08-27)

- fix: last unwatching events time.

## 1.2.1 (2016-08-26)

- fix(status): event status and status style.
- feat(gitlab.version): analytics gitlab version.

## 1.2.0 (2016-08-25)

- feat(activity): support more activity events.
- feat(open): options for new tab or current window.
- fix(badge): click badge icon, goto the right tab.

## 1.1.1 (2016-08-23)

- fix: hide modal after monitor infomations.

## 1.1.0 (2016-08-23)

- feat: new ui for options.

## 1.0.0 (2016-08-23)

- feat: multiple accounts.
- feat: mute and unmute.
- feat: mention @all
- feat: better badge count and link.
- feat: sort events.
- feat: timeago.
- feat: event status.
- fix: Cannot read property 'indexOf' of undefined


## 0.10.2 (2016-08-17)

- fix: mark as read when read issue or MR, match url.

## 0.10.1 (2016-08-16)

- fix(event.type): unwatching event.type mr-comment -> mergerequest-comment.

## 0.10.0 (2016-08-16)

- feat(all messages): support all (unwatching) messages.
- feat(monitor): monitor errors
- feat(stop): stop when page not ready.
- fix(tooltip): hide tooltip when mark as read.
- feat(i18n): l10n tooltip of mark as read.
- feat(badge): badge text show participating messages count.

## 0.9.4 (2016-08-11)

- fix(ga): only stop in notifications.html, watching.html need all projects.
- feat(notifications): tips for mark as read.

## 0.9.3 (2016-08-11)

- feat(ga): stop google analytics when can't connect to google server in China.

## 0.9.2 (2016-08-11)

- feat(footer): copyright, about, feedback.
- feat: delete unused features.
- feat: toc style.

## 0.9.1 (2016-08-08)

- feat(watching): loading and error when fetch all projects in watching page.

## 0.9.0 (2016-08-07)

- feat: restart on save options.
- feat: Add google analytics.
- fix: part chunks send events api.
- fix: limit accounts for now.
- fix: missing old account watching projects.

## 0.8.2 (2016-08-05)

- Refact: better way to get all projects.

## 0.8.1 (2016-08-04)

- Fix: fixed bug when no accounts options.

## 0.8.0 (2016-08-04)

- Feat: watching projects and get events by project. `/projects/:projectId/events`
- Sync notifications.
- Async notifications page.
- participating events.

## 0.1.0 (2016-08-02)

- Support get live event from activity.
