'use strict';

const Messages = require('../messages');

let tabs = getBadge().tabs;

function convertBadgeText(number) {
  if (typeof number === 'string') {
    return number;
  }
  let text = '';
  if (number < 0) {
    text = ' ';
  } else if (number > 0) {
    text = String(number);
  }
  return text;
}


function getBadge() {
  const participatingCount = Messages.length('participating');
  if (participatingCount) {
    return {
      tabs: 'participating',
      count: participatingCount,
      color: '#ff3300',
    };
  }

  const watchingCount = Messages.length('watching');
  if (watchingCount) {
    return {
      tabs: 'watching',
      count: watchingCount,
      color: '#1970A9',
    };
  }

  const allCount = Messages.length('all');
  if (allCount) {
    return {
      tabs: 'all',
      count: -1,
      color: '#999999',
    };
  }

  return {
    tabs: '',
    count: 0,
    color: '#ffffff',
  };
}

function _setBadge(count, bgColor) {
  const badgeText = convertBadgeText(count);
  chrome.browserAction.setBadgeText({ text: badgeText });
  chrome.browserAction.setBadgeBackgroundColor({ color: bgColor });
}

chrome.browserAction.onClicked.addListener(function(tab) {
  window.open('./notifications.html' + (tabs ? '?tab=' + tabs : ''), '__GITLAB_NOTIFICATIONS_LOVE_@lizzie__');
  chrome.tabs.sendRequest(tab.id, {
    type: 'open',
  });
});

module.exports = {
  setBadge: function() {
    const badge = getBadge();
    _setBadge(badge.count, badge.color);
    tabs = badge.tabs;
  },
};
