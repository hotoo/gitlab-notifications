'use strict';

const safe = require('./safe');

function sleep(ms) {
  return function(next) {
    return setTimeout(next, ms);
  };
}

function hasOwnProperty(object, property) {
  return Object.hasOwnProperty.call(object, property);
}

// Unique Array.
// @see http://stackoverflow.com/questions/1960473/unique-values-in-an-array
// @see http://jszen.com/best-way-to-get-unique-values-of-an-array-in-javascript.7.html
function unique(array) {
  return Array.from(new Set(array));
  // return [...new Set(array)];
}

function toArray(list) {
  return Array.prototype.slice.call(list);
}


module.exports = {
  sleep,
  hasOwnProperty,
  unique,
  toArray,
  safe: safe.safe,
  unsafe: safe.unsafe,
};
