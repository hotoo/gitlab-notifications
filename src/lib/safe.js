'use strict';

const MAPPING = [
  { text: /&/g, safetext: /&amp;/g },
  { text: /</g, safetext: /&lt;/g },
  { text: />/g, safetext: /&gt;/g },
  { text: /"/g, safetext: /&quot;/g },
  { text: /'/g, safetext: /&#039;/g },
];

function safe(str) {
  if (!str) return str;
  str = String(str);
  MAPPING.forEach(pairs => {
    str = str.replace(pairs.text, pairs.safetext.source);
  });
  return str;
}

function unsafe(str) {
  if (!str) return str;
  str = String(str);
  for (let i = MAPPING.length - 1, pairs; i >= 0; i--) {
    pairs = MAPPING[i];
    str = str.replace(pairs.safetext, pairs.text.source);
  }
  return str;
}

module.exports = { safe, unsafe };
