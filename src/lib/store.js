'use strict';

const supportLocalStorage = typeof localStorage !== 'undefined';
if (!supportLocalStorage) {
  throw new Error('Not support localStorage.');
}

const isArray = typeOf('Array');
const isObject = typeOf('Object');
// const isString = typeOf('String');
const isBoolean = typeOf('Boolean');
const isNumber = typeOf('Number');
const isRegExp = typeOf('RegExp');
const isFunction = typeOf('Function');
const isUndefined = typeOf('Undefined');

const PREFIX_BOOLEAN = 'store-boolean:';
const PREFIX_NUMBER = 'store-nunber:';
const PREFIX_OBJECT = 'store-object:';
const PREFIX_ARRAY = 'store-array:';

const Store = {
  get: function(key) {
    // default is String.
    let value = localStorage.getItem(key);
    if (isUndefined(value) || value === null) {
      return null;
    } else if (value === PREFIX_BOOLEAN + 'true') {
      return true;
    } else if (value === PREFIX_BOOLEAN + 'false') {
      return false;
    } else if (value.startsWith(PREFIX_NUMBER)) {
      return Number(value.substring(13));
    } else if (value.startsWith(PREFIX_OBJECT)) {
      return JSON.parse(value.substring(13));
    } else if (value.startsWith(PREFIX_ARRAY)) {
      return JSON.parse(value.substring(12));
    }
    return value;
  },
  set: function(key, value) {
    if (isBoolean(value)) {
      value = PREFIX_BOOLEAN + value.toString();
    } else if (isNumber(value)) {
      value = PREFIX_NUMBER + value;
    } else if (isArray(value)) {
      value = PREFIX_ARRAY + JSON.stringify(value);
    } else if (isObject(value)) {
      value = PREFIX_OBJECT + JSON.stringify(value);
    } else if (isRegExp(value) || isFunction(value)) {
      throw new Error('Not support data type.');
    }
    localStorage.setItem(key, value);
  },
  append: function(key, value) {
    let val = Store.get(key);
    if (!isArray(val)) {
      val = [val];
    }
    if (!isArray(value)) {
      value = [value];
    }
    Store.set(key, val.concat(value));
  },
  has: function(key) {
    return localStorage.hasOwnProperty(key);
  },
  remove: function(key) {
    localStorage.removeItem(key);
  },
};

module.exports = Store;

function typeOf(type) {
  return function(object) {
    return Object.prototype.toString.call(object) === '[object ' + type + ']';
  };
}
