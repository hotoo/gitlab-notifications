'use strict';

const re_i18n = /__MSG_(.+?)__/g;

function replace(template) {
  if (typeof template !== 'string') {return '';}
  return template.replace(re_i18n, function($0, $1_msg_name) {
    const msg = chrome.i18n.getMessage($1_msg_name);
    if (!msg) {
      throw new Error($1_msg_name + ' message not exists.');
      // console.error($1_msg_name + ' message not exists.');
      // return $0;
    }
    return msg;
  });
}

function get(key, substitutions) {
  return chrome.i18n.getMessage(key, substitutions);
}

function dom() {
  document.title = replace(document.title);
}

module.exports = {
  replace,
  get,
  dom,
};
