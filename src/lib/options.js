'use strict';

const Store = require('./store');
const options_prefix = 'options.';

const Options = {
  init: function(version, options) {
    const options_version_name = options_prefix + 'version';

    // 如果已经初始化，退出。
    if (version === Store.get(options_version_name)) {return true;}

    for (let name in options) {
      // 如果该选项已经设置，则继承使用用户的设置。
      if (Store.has(options_prefix + name)) {continue;}
      Store.set(options_prefix + name, options[name]);
    }
    Store.set(options_version_name, version);
    return true;
  },
  get: function(name) {
    return Store.get(options_prefix + name);
  },
  set: function(name, value) {
    // 只能设置已有的配置选项。
    // if (!Store.has(name)) { return false; }
    Store.set(options_prefix + name, value);
  },
};

module.exports = Options;
