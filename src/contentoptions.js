'use strict';

const Options = {
  get: function(key) {
    return new Promise(function(resolve) {
      chrome.extension.sendRequest({type: 'get-options', key: key}, function(option) {
        resolve(option);
      });
    });
  },
};

module.exports = Options;
