'use strict';

require('babel-polyfill');

const co = require('co');
const extension_version = require('./manifest.json').version;
const util = require('./lib/util');
const badge = require('./lib/badge');
const Options = require('./lib/options');
const GitLabEvents = require('./gitlab-events');
const Messages = require('./messages');
const monitor = require('./lib/monitor');
const upgrade = require('./upgrade');


co(function*() {

  const oldVersion = Options.get('version') || '0.0';
  if (oldVersion.startsWith('0.') && extension_version.startsWith('1.')) {
    upgrade.v0_to_v1();
  }

  let timers;

  chrome.extension.onRequest.addListener((request, sender, sendResponse) => {
    switch (request.type) {
    case 'get-options':
      sendResponse(Options.get(request.key));
      break;
    case 'mark-as-read':
      Messages.read(request.key);
      badge.setBadge();
      break;
    case 'is-mute':
      sendResponse(Messages.isMute(request.key));
      break;
    case 'mark-as-mute':
      Messages.mute(request.key);
      sendResponse(Messages.isMute(request.key));
      break;
    case 'mark-as-unmute':
      Messages.unmute(request.key);
      sendResponse(Messages.isMute(request.key));
      break;
    case 'set-badge':
      badge.setBadge();
      break;
    case 'close':
      break;
    case 'restart':
      if (timers && timers.length) {
        timers.forEach(timer => timer.stop());
      }
      co(function*() {
        yield start();
        sendResponse({
          status: 200,
          body: 'restart success.',
        });
      }).catch(function(err) {
        throw new Error(err);
      });
      break;
    default:
      break;
    }
  });

  yield start();

  function* start() {
    const gitlabAccounts = Options.get('gitlab.accounts') || [];
    timers = [];
    yield gitlabAccounts.map(account => initBackground(account));
  }

  function* initBackground(account) {

    // init mentions keyword.
    const mentions = account.aliasName.split(/[,，]\s*/)
      .map(name => name.trim())
      .filter(name => !!name);
    mentions.push('@' + account.userName, '@all');
    account.mentions = util.unique(mentions);

    badge.setBadge();

    const gitlabEvents = new GitLabEvents(account);
    gitlabEvents.on('events', function(events) {
      events.forEach(event => {
        const mentioned = account.mentions.some(name => {
          return event.body.indexOf(name) >= 0 ||
                 event.title.indexOf(name) >= 0;
        });
        const fromMe = account.userName === event.author_username;
        const assignToMe = account.userName === event.assignee_username;
        event.participating = mentioned || fromMe || assignToMe;

        if (fromMe) {
          Messages.update(event);
        } else {
          Messages.add(event);
        }
      });

      badge.setBadge();

      const notification_events = events.filter(event => {
        if (account.userName === event.author_username) {
          return false;
        }
        return account.notification === 'all' ||
               account.notification === 'watching' && !!event.watching ||
               account.notification === 'participating' && !!event.participating;
      });

      if (notification_events.length > 3) {
        chrome.notifications.create('./notifications.html', {
          type: 'basic',
          iconUrl: './img/gitlab_logo_128.png',
          title: notification_events.length + ' new events',
          message: 'You have ' + notification_events.length + ' new events.',
          priority: 0,
        });
      } else {

        notification_events.forEach(event => {
          let title;
          let message;
          const notification_id = event.url;
          switch (event.type) {
          case 'mergerequest-opened':
            title = event.author_name + ': ' + event.title;
            message = event.project_path + '!' + event.mr_iid + ' opened.\n' + event.body;
            break;
          case 'mergerequest-reopened':
            title = event.author_name + ': ' + event.title;
            message = event.project_path + '!' + event.mr_iid + ' reopened.';
            break;
          case 'mergerequest-closed':
            title = event.author_name + ': ' + event.title;
            message = event.project_path + '!' + event.mr_iid + ' closed.';
            break;
          case 'mergerequest-accepted':
            title = event.author_name + ': ' + event.title;
            message = event.project_path + '!' + event.mr_iid + ' accepted.';
            break;
          case 'mergerequest-comment':
            title = event.author_name + ': Re ' + event.title;
            message = event.project_path + '!' + event.mr_iid + '\n' + event.body;
            break;

          case 'issue-opened':
            title = event.author_name + ': ' + event.title;
            message = event.project_path + '#' + event.issue_iid + ' opened.\n' + event.body;
            break;
          case 'issue-reopened':
            title = event.author_name + ': ' + event.title;
            message = event.project_path + '#' + event.issue_iid + ' reopened.';
            break;
          case 'issue-closed':
            title = event.author_name + ': ' + event.title;
            message = event.project_path + '#' + event.issue_iid + ' closed.';
            break;
          case 'issue-comment':
            title = event.author_name + ': Re ' + event.title;
            message = event.project_path + '#' + event.issue_iid + '\n' + event.body;
            break;
          default:
            return;
          }

          chrome.notifications.create(notification_id, {
            type: 'basic',
            iconUrl: event.author_avatar,
            title: title,
            message: message,
            priority: 0,
          }, function() {
          });
        });

      }
    }).on('account.state.change', state => {
      console.log('status change', state);
      const gitlabAccounts = Options.get('gitlab.accounts') || [];
      gitlabAccounts.forEach(acc => {
        if (account.serverUrl === acc.serverUrl) {
          acc.state = state;
        }
      });
      Options.set('gitlab.accounts', gitlabAccounts);
    });
    timers.push(gitlabEvents);
    yield gitlabEvents.start();
  }

}).catch(function(err) {
  monitor.error(err);
});

chrome.notifications.onClicked.addListener(function(notification_id) {
  chrome.notifications.clear(notification_id);
  chrome.tabs.create({
    url: notification_id,
  });
});
chrome.notifications.onClosed.addListener(function(notification_id) {
  chrome.notifications.clear(notification_id);
});

// GitLabEvents.on('debug', function(data) {
  // chrome.tabs.getSelected(null, function(tab){
    // chrome.tabs.sendRequest(tab.id, {
      // type: 'debug',
      // data: data,
    // });
  // });
// });

// chrome.browserAction.setIcon({
  // path: '',
// }, function(callback) {
// })

const defaultOptions = {
  'general.sound.enable': true,
  'general.popup.enable': true,
  'gitlab.servers': [
    'https://gitlab.com/',
    'http://gitlab.alipay-inc.com/',
    'http://gitlab.alibaba-inc.com/',
  ],
};
// init default options.
Options.init(extension_version, defaultOptions);
