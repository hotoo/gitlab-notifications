'use strict';

const $ = require('jquery');
const url = require('url');
const querystring = require('querystring');
const util = require('./lib/util');
const Metadata = require('./lib/metadata');

const PER_PAGE = 20;
const TIMEOUT = 60000;
const RE_COMMENT_MR_EVENTS = /^commented on merge request #(\d+)/;
const RE_COMMENT_ISSUE_EVENTS = /^commented\son\sissue\s#(\d+)/;

const RE_PRIVATE_TOKEN = [
  /gon\.api_token="([^"]+)";/,
  /<input type="text" name="private-token" id="private-token" value="([^"]+)" /,
];
// GitLab
const RE_USER_NAMES = [
  // GitLab Community Edition 8.3.2
  /<a class="sidebar-user" title="Profile" href="\/u\/([^"]+)">/,
  /<div class='username'>([^<]+)<\/div>/,
  // GitLab Enterprise Edition 8.11.4-ee
  /<a class="profile-link" aria-label="Profile" data-user="([^"]+)" href="\/u\/([^"]+)">Profile<\/a>/,
  /<input required="required" class="form-control" type="text" value="([^"]+)" name="user\[username\]" id="user_username" \/>/,
  /<a class="profile-link" aria-label="Profile" data-user="([^"]+)" href="\/hotoo">Profile<\/a>/,
];

class GitLabAPI {
  constructor(options) {
    this.serverUrl = options.server;
    this.apiPath = options.path || '/api/v3';
    this.apiUrl = this.serverUrl + this.apiPath;
    this.privateToken = options.token;
    this._metadata = new Metadata(this.serverUrl);
  }

  // Get live events.
  * liveEvents(limit, offset) {
    const eventsData = yield $.ajax({
      url: this.serverUrl + '/dashboard/activity.json',
      data: {
        limit: limit || PER_PAGE,
        offset: offset || 0,
      },
      type: 'GET',
      dataType: 'json',
      timeout: TIMEOUT,
      headers: {
        'PRIVATE-TOKEN': this.privateToken,
        // 'Cookie': 'event_filter=merged%2Ccomments',
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'X-Requested-With': 'XMLHttpRequest',
        'Cache-Control': 'no-cache',
        // 'Referer': this.serverUrl,
      },
    });

    const events = parseLiveEvents(eventsData, this.serverUrl);
    return events;
  }

  // Get GitLab Version
  static * gitlabVersion(serverUrl) {
    try {
      const html = yield $.ajax({
        url: serverUrl + '/help',
        type: 'GET',
        timeout: TIMEOUT,
      });
      const RE_GitLab_Version = /<h1>\s*GitLab[^<]+<span>([^<]+)<\/span>\s*<small>[0-9a-f]+<\/small>/;

      const m = RE_GitLab_Version.exec(html);
      return m ? m[1] : 'n/a';
    } catch (er) {
      throw {
        type: 'ServiceUnavailable',
        statusCode: 503,
        message: 'GET GitLab server error: ' + serverUrl,
      };
    }
  }

  * gitlabVersion() {
    const version = yield GitLabAPI.gitlabVersion(this.serverUrl);
    return version;
  }

  /**
   * Get stared projects.
   */
  * starredProjects() {
    return $.ajax({
      url: this.apiUrl + '/projects/starred',
      type: 'GET',
      dataType: 'json',
      timeout: TIMEOUT,
      headers: {
        'PRIVATE-TOKEN': this.privateToken,
      },
    });
  }

  /**
   * Get Owned projects.
   */
  * ownedProjects() {
    return $.ajax({
      url: this.apiUrl + '/projects/owned',
      type: 'GET',
      dataType: 'json',
      timeout: TIMEOUT,
      headers: {
        'PRIVATE-TOKEN': this.privateToken,
      },
    });
  }

  * project(project_path) {
    return $.ajax({
      url: this.apiUrl + '/projects/' + encodeURIComponent(project_path),
      type: 'GET',
      dataType: 'json',
      timeout: TIMEOUT,
      headers: {
        'PRIVATE-TOKEN': this.privateToken,
      },
    });
  }

  /**
   * Get all projects.
   *
   * @return {Array}
   */
  * projects(refresh = false) {
    if (!refresh) {
      let ALL_PROJECTS = this._metadata.get('projects') || [];
      if (ALL_PROJECTS.length) {
        return ALL_PROJECTS;
      }
    }

    let project_list = [];

    function pushPartProjects(plist) {
      project_list.push.apply(project_list, plist.projects);
    }

    const projectList = yield getProjects(this.apiUrl, this.privateToken, 1);
    project_list.push.apply(project_list, projectList.projects);
    if (projectList.pages > 1) {
      const moreProjectList = Array.apply(null, new Array(projectList.pages - 1)).map((item, index) => {
        return getProjects(this.apiUrl, this.privateToken, index + 2);
      });
      do {
        const partlist = yield moreProjectList.splice(0, 5);
        partlist.forEach(pushPartProjects);
      } while (moreProjectList.length);
    }

    this._metadata.set('projects', project_list);
    return project_list;
  }

  /**
   * 根据仓库简写名解析出仓库对应信息 `group/repo-name`.
   *
   * @param {String} shortName
   * @return {Object}
   */
  * parse(shortName) {
    const ALL_PROJECTS = yield this.projects();
    const proj = ALL_PROJECTS.filter(function(project) {
      if (project.path_with_namespace === shortName) {
        return true;
      }
    });
    if (!proj.length) {
      let project;
      try {
        project = yield this.project(shortName);
        ALL_PROJECTS.push(project);
        this._metadata.set('projects', ALL_PROJECTS);
        return project;
      } catch (ex) {
        console.error('get project info error:', shortName);
        return null;
      }
    } else {
      return proj[0];
    }
  }

  /**
   * Get project events.
   *
   * @see [Get project events](https://github.com/gitlabhq/gitlabhq/blob/master/doc/api/projects.md#get-project-events)
   *
   * @param {String} projectID
   * @return {Array} events
   */
  * projectEvents(projectID, page = 1, per_page = PER_PAGE) {
    return $.ajax({
      url: this.apiUrl + '/projects/' + projectID + '/events',
      data: {
        per_page: per_page,
        page: page,
      },
      type: 'GET',
      dataType: 'json',
      timeout: TIMEOUT,
      headers: {
        'PRIVATE-TOKEN': this.privateToken,
      },
    });
  }


  /**
   * Get project issues.
   *
   * @param {String} projectID
   * @return {Object} issues
   */
  * projectIssues(projectID) {
    return $.ajax({
      url: this.apiUrl + '/projects/' + projectID + '/issues',
      data: {
        order_by: 'created_at',
        sort: 'desc',
      },
      type: 'GET',
      dataType: 'json',
      timeout: TIMEOUT,
      headers: {
        'PRIVATE-TOKEN': this.privateToken,
      },
    });
  }

  /**
   * Get issue entry by project id and issue id.
   *
   * @param {String} projectID
   * @param {String} issueIID
   * @return {Array<Object>} issue entry list, the list has one entry of issue.
   */
  * issue(projectID, issueID) {
    return $.ajax({
      url: this.apiUrl + '/projects/' + projectID + '/issues',
      data: {
        iid: issueID,
      },
      type: 'GET',
      dataType: 'json',
      timeout: TIMEOUT,
      headers: {
        'PRIVATE-TOKEN': this.privateToken,
      },
    });
  }

  /**
   * Get issue entry by project id and issue id.
   *
   * @param {String} projectID
   * @param {String} issueID
   * @return {Object} issue entry.
   */
  * issue_1(projectID, issueID) {
    return $.ajax({
      url: this.apiUrl + '/projects/' + projectID + '/issues/' + issueID,
      type: 'GET',
      dataType: 'json',
      timeout: TIMEOUT,
      headers: {
        'PRIVATE-TOKEN': this.privateToken,
      },
    });
  }

  /**
   * Get Comments for Issue.
   *
   * @param {Number} projectID
   * @param {Number} issueIID issue `iid`, not `id`.
   * @return {Array} Notes (Comments).
   */
  * issueNotes(projectID, issueIID) {
    const issue = yield this.issue(projectID, issueIID);
    if (!issue || issue.length !== 1) {
      throw new Error('Not found issue #' + issueIID + ' in project#' + projectID);
    }
    return $.ajax({
      // /projects/:id/issues/:issue_id/notes
      url: this.apiUrl + '/projects/' + projectID + '/issues/' + issue[0].id + '/notes',
      // data: {
        // issue_id: issueID,
        // note_id: '101870',
      // },
      type: 'GET',
      dataType: 'json',
      timeout: TIMEOUT,
      headers: {
        'PRIVATE-TOKEN': this.privateToken,
      },
    });
  }


  // GET /projects/:id/issues/:issue_id/notes/:note_id
  * issueNote(projectID, issueIID, noteID) {
    const issue = yield this.issue(projectID, issueIID);
    if (!issue || issue.length !== 1) {
      throw new Error('Not found issue #' + issueIID + ' in project#' + projectID);
    }
    return $.ajax({
      url: this.apiUrl + '/projects/' + projectID + '/issues/' + issue[0].id + '/notes/' + noteID,
      type: 'GET',
      dataType: 'json',
      timeout: TIMEOUT,
      headers: {
        'PRIVATE-TOKEN': this.privateToken,
      },
    });
  }

  /**
   * Get issue entry by project id and issue id.
   *
   * @param {String} projectID
   * @param {String} issueID
   * @return {Array<Object>} issue entry list, has one of the mr entry.
   */
  * mergeRequest(projectID, mrID) {
    return $.ajax({
      url: this.apiUrl + '/projects/' + projectID + '/merge_requests',
      data: {
        iid: mrID,
      },
      type: 'GET',
      dataType: 'json',
      timeout: TIMEOUT,
      headers: {
        'PRIVATE-TOKEN': this.privateToken,
      },
    });
  }

  /**
   * Get merge request entry by project id and mr id.
   *
   * @param {String} projectID
   * @param {String} mrID
   * @return {Object} merge request entry.
   */
  * mergeRequest_1(projectID, mrID) {
    return $.ajax({
      url: this.apiUrl + '/projects/' + projectID + '/merge_request/' + mrID,
      type: 'GET',
      dataType: 'json',
      timeout: TIMEOUT,
      headers: {
        'PRIVATE-TOKEN': this.privateToken,
      },
    });
  }

  // GET /projects/:id/merge_requests/:merge_request_id/notes
  * mergeRequestNotes(projectID, mrIID) {
    const mr = yield this.mergeRequest(projectID, mrIID);
    if (!mr || mr.length !== 1) {
      throw new Error('Not found issue #' + mrIID + ' in project#' + projectID);
    }
    return $.ajax({
      url: this.apiUrl + '/projects/' + projectID + '/merge_requests/' + mr[0].id + '/notes',
      type: 'GET',
      dataType: 'json',
      timeout: TIMEOUT,
      headers: {
        'PRIVATE-TOKEN': this.privateToken,
      },
    });
  }


  // GET /projects/:id/merge_requests/:merge_request_id/notes/:note_id
  * mergeRequestNote(projectID, mrIID, noteID) {
    const mr = yield this.mergeRequest(projectID, mrIID);
    if (!mr || mr.length !== 1) {
      throw new Error('Not found merge request #' + mrIID + ' in project#' + projectID);
    }
    return $.ajax({
      url: this.apiUrl + '/projects/' + projectID + '/merge_requests/' + mr[0].id + '/notes/' + noteID,
      type: 'GET',
      dataType: 'json',
      timeout: TIMEOUT,
      headers: {
        'PRIVATE-TOKEN': this.privateToken,
      },
    });
  }


  /**
   * Comment on MR
   *
   * API: POST (/projects/:id/merge_request/:merge_request_id/comments)
   */
  * commentMR(projectID, mrIID, note) {
    const mr = yield this.mergeRequest(projectID, mrIID);
    return $.ajax({
      url: this.apiUrl + '/projects/' + projectID + '/merge_request/' + mr[0].id + '/comments',
      data: {
        note: note,
      },
      type: 'POST',
      dataType: 'json',
      timeout: TIMEOUT,
      headers: {
        'PRIVATE-TOKEN': this.privateToken,
      },
    });
  }

  static account(serverUrl) {
    return function(next) {
      return $.get(serverUrl + '/profile/account').then(function(html) {
        if (/<title>Sign in\b[^<]*<\/title>/i.test(html)) {
          const err = {
            type: 'Unauthorized',
            statusCode: 401,
            message: serverUrl + '/users/sign_in',
            'location': serverUrl + '/users/sign_in',
          };
          return next(err);
        }

        let userName = '';
        let privateToken = '';

        RE_PRIVATE_TOKEN.some(re => {
          const m = re.exec(html);
          if (m) {
            privateToken = m[1];
            return true;
          }
        });

        RE_USER_NAMES.some(re => {
          const n = re.exec(html);
          if (n) {
            userName = n[1];
            return true;
          }
        });

        next(null, {
          userName,
          privateToken,
        });

      }).catch(ex => {
        ex = {
          type: 'ServiceUnavailable',
          statusCode: 503,
          message: 'GET GitLab server error: ' + serverUrl,
        };
        return next(ex);
      });
    };
  }

}

module.exports = GitLabAPI;

/*
Link:
  <http://gitlab.alipay-inc.com/api/v3/projects?page=1&per_page=20>; rel="prev",
  <http://gitlab.alipay-inc.com/api/v3/projects?page=3&per_page=20>; rel="next",
  <http://gitlab.alipay-inc.com/api/v3/projects?page=1&per_page=20>; rel="first",
  <http://gitlab.alipay-inc.com/api/v3/projects?page=28&per_page=20>; rel="last"
*/
const RE_LINK = /^<([^>]+)>; *rel="([^"]+)"$/;
function parseLinkHeader(link) {
  const meta = {};
  const links = link.split(/, */).map(li => li.trim()).filter(li => !!li);
  links.forEach(li => {
    const m = RE_LINK.exec(li);
    if (!m) return;
    const ref = m[1];
    const u = url.parse(ref);
    const rel = m[2];
    meta[rel] = {
      rel: rel,
      ref: ref,
      query: querystring.decode(u.query),
    };
  });
  return meta;
}
/*
 * Get projects by page
 *
 * @param {Number} page
 * @return {Array} projects
 */
function getProjects(server, token, page) {
  return function(next) {
    return $.ajax({
      url: server + '/projects',
      data: {
        per_page: PER_PAGE,
        page: page,
        order_by: 'name',
        sort: 'asc',
      },
      type: 'GET',
      dataType: 'json',
      timeout: TIMEOUT,
      headers: {
        'PRIVATE-TOKEN': token,
      },
    }).done(function(projects, textStatus, xhr) {
      const linkHeader = xhr.getResponseHeader('Link');
      const pages = linkHeader ? Number(parseLinkHeader(linkHeader).last.query.page) : 1;
      next(null, {
        pages,
        projects,
      });
    }).fail(function(xhr, status, err) {
      next(err);
    });
  };
}

const RE_ISSUE_NOTE = /^\/([\w.-]+\/[\w.-]+)\/issues\/(\d+)#note_(\d+)/;
const RE_ISSUE = /^\/([\w.-]+\/[\w.-]+)\/issues\/(\d+)/;
const RE_MR_NOTE = /^\/([\w.-]+\/[\w.-]+)\/merge_requests\/(\d+)#note_(\d+)/;
const RE_MR = /^\/([\w.-]+\/[\w.-]+)\/merge_requests\/(\d+)/;
function parseLiveEvents(eventsData, host) {
  if (!eventsData.count) {
    return [];
  }
  const html = eventsData.html.replace(/ src="\//g, ' src="' + host + '/');

  return util.toArray($('<div>' + html + '</div>').find('.event-item')).map(function(element) {
    const event_time = Date.parse($(element).find('.event-item-timestamp > time').attr('datetime'));

    const event_type = $(element).find('.event_label').text().trim().replace(/\s+/g, ' ');
    const author_name = $(element).find('.author_name > a').text().trim();
    const author_id = $(element).find('.author_name > a').attr('href').substring(3);
    const event_id = author_id + '-' + event_type + '-' + event_time;

    let match = RE_COMMENT_ISSUE_EVENTS.exec(event_type);
    if (match) {
      const event_info = $(element).find('.event_label > a').attr('href');
      const n = RE_ISSUE_NOTE.exec(event_info);
      return {
        id: match[1],
        type: 'issue-comment',
        author_id: author_id,
        author_name: author_name,
        event_id: event_id,
        project_path: n[1],
        project_url: host + '/' + n[1],
        issue_iid: n[2],
        note_id: n[3],
        timestamp: event_time,
        key: host + '/' + n[1] + '/issues/' + n[2],
        url: host + '/' + n[1] + '/issues/' + n[2] + '#note_' + n[3],
      };
    }

    match = RE_COMMENT_MR_EVENTS.exec(event_type);
    if (match) {
      const event_info = $(element).find('.event_label > a').attr('href');
      const n = RE_MR_NOTE.exec(event_info);

      return {
        id: match[1],
        type: 'mergerequest-comment',
        author_id: author_id,
        author_name: author_name,
        event_id: event_id,
        project_path: n[1],
        project_url: host + '/' + n[1],
        mr_iid: n[2],
        note_id: n[3],
        timestamp: event_time,
        key: host + '/' + n[1] + '/merge_requests/' + n[2],
        url: host + '/' + n[1] + '/merge_requests/' + n[2] + '#note_' + n[3],
      };
    }

    if (event_type === 'opened issue') {
      const event_info = $(element).find('.event-title > strong > a').attr('href');
      const n = RE_ISSUE.exec(event_info);
      return {
        type: 'issue-opened',
        key: host + event_info,
        url: host + event_info,
        title: $(element).find('.event-note').text().trim(),
        body: '',
        project_path: n[1],
        project_url: host + '/' + n[1],
        mr_iid: n[2],
        author_name: $(element).find('.author_name > a').text(),
        author_username: $(element).find('.author_name > a').attr('href').replace('/u/', ''),
        author_avatar: $(element).find('img.avatar').attr('src'),
        timestamp: event_time,
      };
    }

    if (event_type === 'closed issue') {
      const event_info = $(element).find('.event-title > strong > a').attr('href');
      const n = RE_ISSUE.exec(event_info);
      return {
        type: 'issue-closed',
        key: host + event_info,
        url: host + event_info,
        title: $(element).find('.event-note').text().trim(),
        body: '',
        project_path: n[1],
        project_url: host + '/' + n[1],
        mr_iid: n[2],
        author_name: $(element).find('.author_name > a').text(),
        author_username: $(element).find('.author_name > a').attr('href').replace('/u/', ''),
        author_avatar: $(element).find('img.avatar').attr('src'),
        timestamp: event_time,
      };
    }

    if (event_type === 'opened merge request') {
      const event_info = $(element).find('.event-title > strong > a').attr('href');
      const n = RE_MR.exec(event_info);
      return {
        type: 'mergerequest-opened',
        key: host + event_info,
        url: host + event_info,
        title: $(element).find('.event-note').text().trim(),
        body: '',
        project_path: n[1],
        project_url: host + '/' + n[1],
        mr_iid: n[2],
        author_name: $(element).find('.author_name > a').text(),
        author_username: $(element).find('.author_name > a').attr('href').replace('/u/', ''),
        author_avatar: $(element).find('img.avatar').attr('src'),
        timestamp: event_time,
      };
    }

    // accept merge request.
    if (event_type === 'accepted merge request') {
      const event_info = $(element).find('.event-title > strong > a').attr('href');
      const n = RE_MR.exec(event_info);
      return {
        type: 'mergerequest-accepted',
        key: host + event_info,
        url: host + event_info,
        title: $(element).find('.event-note').text().trim(),
        body: '',
        project_path: n[1],
        project_url: host + '/' + n[1],
        mr_iid: n[2],
        author_username: $(element).find('.author_name > a').attr('href').replace('/u/', ''),
        author_name: $(element).find('.author_name > a').text(),
        author_avatar: $(element).find('img.avatar').attr('src'),
        timestamp: event_time,
      };
    }

    // mergerequest-closed
    if (event_type === 'closed merge request') {
      const event_info = $(element).find('.event-title > strong > a').attr('href');
      const n = RE_MR.exec(event_info);
      return {
        type: 'mergerequest-closed',
        key: host + event_info,
        url: host + event_info,
        title: $(element).find('.event-note').text().trim(),
        body: '',
        project_path: n[1],
        project_url: host + '/' + n[1],
        mr_iid: n[2],
        author_username: $(element).find('.author_name > a').attr('href').replace('/u/', ''),
        author_name: $(element).find('.author_name > a').text(),
        author_avatar: $(element).find('img.avatar').attr('src'),
        timestamp: event_time,
      };
    }

  })
  .filter(event => !!event);
}
