'use strict';

class Timeouter {
  constructor(timeout, maxTimeout) {
    this._timeout = timeout || 1000; // current timeout.
    this._TIMEOUT = this._timeout; // base timeout, fixed.
    if (maxTimeout >= this._TIMEOUT) {
      this._maxTimeout = maxTimeout || 10 * this._TIMEOUT;
    } else {
      throw new Error('max timeout must be great than timeout.');
    }
  }

  valueOf() {
    return this._timeout;
  }

  inc() {
    let time = this._timeout + this._TIMEOUT;
    if (time > this._maxTimeout) {
      time = this._maxTimeout;
    }
    this._timeout = time;
  }

  reset() {
    this._timeout = this._TIMEOUT;
  }
}

module.exports = Timeouter;
