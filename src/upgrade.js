/**
 * 升级插件过程中，可能需要执行的一些数据订正类的工作。
 */
'use strict';

const Messages = require('./messages');
const Store = require('./lib/store');
const Metadata = require('./lib/metadata');
const Options = require('./lib/options');

const PREFIX_DATA = 'notification.data';
const prefix_metadata_projects = 'metadata.projects';
const accounts = Options.get('gitlab.accounts') || [];

const watchingProjects = [];
accounts.forEach(account => {
  watchingProjects.push.apply(watchingProjects, account.watching_projects);
});

// Upgrade 0.x to 1.x
function upgrade_v0_to_v1() {
  const messages = Messages.list({type: '*'});
  if (messages && messages.length) {
    messages.forEach(msg => {
      msg.read = false;
      msg.watching = watchingProjects.findIndex(proj => proj.web_url === msg.project_url) >= 0;
    });
    Store.set(PREFIX_DATA, messages);
  }

  const all_projects = Store.get(prefix_metadata_projects);
  if (all_projects && all_projects.length) {
    const metadata = new Metadata(accounts[0].serverUrl);
    metadata.set('projects', all_projects);
    Store.remove(prefix_metadata_projects);
  }
}

module.exports = {
  v0_to_v1: upgrade_v0_to_v1,
};
