'use strict';

require('babel-polyfill');

require('./options_page.less');
const GitLabAPI = require('./gitlab-api');
const co = require('co');
const $ = require('jquery');
const util = require('./lib/util');
// const Form = require('./form');
const Options = require('./lib/options');
const i18n = require('./lib/i18n');
const Metadata = require('./lib/metadata');
// const GitLabAccountUI = require('./options-gitlab-account');
const tpl = require('./options.atpl');
const tplModal = require('./options-modal.atpl');
const tplProjects = require('./watching.atpl');
const monitor = require('./lib/monitor');

window.jQuery = $;
require('bootstrap');

const max_accounts = require('./config').max_accounts;

const STORE_KEY_ACCOUNT = 'gitlab.accounts';
const accounts = Options.get(STORE_KEY_ACCOUNT) || [];
const openTarget = Options.get('open.target') || '_self';

const RE_URL = /^https?:\/\/\w+\.\w+/;

let currAccount;
let currAccountIndex;
let curr_projects = [];
let edit_mode;

const accountsCatchedWatching = {};
accounts.forEach(account => accountsCatchedWatching[account.serverUrl] = account);

function namedUrl(url) {
  return url.replace(/^https?:\/\//, '').split('.').slice(-2).join('.');
}

function renderAccounts(accounts) {
  const html = i18n.replace(tpl({
    menu: 'options',
    max_accounts,
    accounts,
    openTarget,
    helper: {
      namedUrl,
    },
  }));
  $('#wrapper').html(html);
}
function renderModal() {
  const htmlModal = i18n.replace(tplModal({ }));
  $('#modal').html(htmlModal);
}
renderAccounts(accounts);
renderModal();


function renderTableView(serverUrl, datas) {
  const watchedCache = {};
  let watchingProjects;
  watchingProjects = currAccount && currAccount.watching_projects ? currAccount.watching_projects : [];
  watchingProjects.forEach(proj => {
    watchedCache[serverUrl + '/' + proj.path_with_namespace] = true;
  });
  const data = datas
  .filter(item => item.issues_enabled || item.merge_requests_enabled)
  .sort((a, b) => {
    const wa = util.hasOwnProperty(watchedCache, a.web_url);
    const wb = util.hasOwnProperty(watchedCache, b.web_url);
    if (wa && !wb) return -1;
    if (!wa && wb) return 1;
    return a.path_with_namespace.localeCompare(b.path_with_namespace);
  });
  data.forEach(item => {
    item.watching = util.hasOwnProperty(watchedCache, item.web_url);
  });

  const html = i18n.replace(tplProjects({
    serverUrl,
    data,
    status: 'ok',
  }));
  $('#table-project-list').html(html);
}


$(document.body).on('click', '[data-action=delete]', function() {
  const index = parseInt($(this).attr('data-index'), 10);
  accounts.splice(index, 1);
  Options.set(STORE_KEY_ACCOUNT, accounts);
  renderAccounts(accounts);
});


function showWaitButton() {
  $('#btn-retry').text(i18n.get('options_btn_Wait')).prop('disabled', true).show();
  $('#btn-nextStep').hide();
}
function showRetryButton() {
  $('#btn-retry').text(i18n.get('options_btn_Retry')).prop('disabled', false).show();
  $('#btn-nextStep').hide();
}
function showNextButton() {
  $('#btn-retry').hide();
  $('#btn-nextStep').prop('disabled', false).show();
}
function _getAllProjects(serverUrl, privateToken) {
  showWaitButton();
  getProjects(serverUrl, privateToken, function(err, project_list) {
    if (err) {
      showRetryButton();
    } else {
      curr_projects = project_list;
      const metadata = new Metadata(serverUrl);
      metadata.set('projects', project_list);
      renderTableView(serverUrl, project_list);
      showNextButton();
    }
  });
}

$('#btn-retry').click(() => {
  const serverUrl = $('#input-server-url').val();
  const privateToken = $('#input-private-token').val();
  _getAllProjects(serverUrl, privateToken);
});

function _getAccount(serverUrl) {
  $('#help-server-url').hide();
  $('#help-server-url').parent().removeClass('has-error has-feedback');

  co(function* () {
    try {
      const [ account, gitlabVersion ] = yield [
        GitLabAPI.account(serverUrl),
        GitLabAPI.gitlabVersion(serverUrl),
      ];
      console.log('gitlab version', gitlabVersion, account);

      if (account.privateToken) {
        $('#input-private-token').val(account.privateToken);
        _getAllProjects(serverUrl, account.privateToken);
      } else {
        monitor.log(gitlabVersion, 'GET private token failed.');
      }
      if (account.userName) {
        $('#input-username').val(account.userName);
      } else {
        monitor.log(gitlabVersion, 'GET user name failed.');
      }
    } catch (ex) {
      if (ex.statusCode === 401) { // Unauthorized
        monitor.log('GitLab account unauthorized');
        const btnRetry = $('<button type="button" class="btn btn-default btn-xs btn-sign-in-and-retry">' + i18n.get('options_btn_Retry') + '</button>');
        btnRetry.click(function() {
          _getAccount(serverUrl);
        });
        $('#help-server-url')
          .html('<a href="' + ex.location + '" target="_blank">Sign in please.</a>')
          .append(btnRetry)
          .show();
        $('#help-server-url').parent().addClass('has-error has-feedback');
      } else if (ex.statusCode === 503) { // ServiceUnavailable
        monitor.error(ex, 'GET GitLab server error');
        $('#help-server-url').html('Can\'t connect GitLab server <i>' + serverUrl + '</i>.').show();
        $('#help-server-url').parent().addClass('has-error has-feedback');
        $('#btn-nextStep').prop('disabled', true).show();
      }
    }
  });
}

$('#input-server-url').change(function() {
  let serverUrl = this.value;
  if (!RE_URL.test(serverUrl)) { return; }
  serverUrl = serverUrl.trim().replace(/\/+$/, '');
  const serverExists = accounts.findIndex(account => account.serverUrl === serverUrl) >= 0;
  if (serverExists) {
    $(this).parent().addClass('has-error has-feedback');
    $('#help-server-url').html('Server <i>' + serverUrl + '</i> is exists.').show();
    $('#help-server-url').parent().addClass('has-error has-feedback');
    return false;
  } else {
    $(this).parent().removeClass('has-error has-feedback');
    $('#help-server-url').hide();
  }
  $('#input-account-name').val(namedUrl(serverUrl));
  $('#help-private-token').attr('href', serverUrl + '/profile/account');

  _getAccount(serverUrl);
});

$('#input-private-token').change(function() {
  const privateToken = this.value;
  $('#btn-nextStep').prop('disabled', !privateToken);
});
$('#form-add-account').submit(function() {
  const serverUrl = $('#input-server-url').val().trim().replace(/\/+$/, '');
  const name = $('#input-account-name').val();
  const aliasName = $('#input-aliasname').val();
  const userName = $('#input-username').val();
  const privateToken = $('#input-private-token').val();
  const notification = $('#input-notification').val();

  const serverIndex = accounts.findIndex(account => account.serverUrl === serverUrl);
  const serverExists = serverIndex >= 0;
  if (edit_mode === 'add') {
    if (serverExists) {
      $('#help-server-url').html('Server "' + serverUrl + '" exists.').show();
      $('#help-server-url').parent().addClass('has-error has-feedback');
      return false;
    } else {
      $('#help-server-url').hide();
      $('#help-server-url').parent().removeClass('has-error has-feedback');
    }
  } else if (edit_mode === 'edit') {
    if (serverExists) {
      $('#help-server-url').hide();
      $('#help-server-url').parent().removeClass('has-error has-feedback');
    } else {
      $('#help-server-url').html('Server "' + serverUrl + '" is not exists.').show();
      $('#help-server-url').parent().addClass('has-error has-feedback');
      return false;
    }
  }

  $('#form-add-account').hide();
  $('#form-watching-project').show();

  currAccount = {
    serverUrl,
    name,
    aliasName,
    userName,
    privateToken,
    notification,
  };

  return false;
});
$('#btn-previousStep').click(function() {
  $('#form-watching-project').hide();
  $('#form-add-account').show();
});
$('#form-watching-project').submit(function() {
  $('#btn-save').prop('disabled', true);
  const checked = $('#table-project-list input[type=checkbox][name=watch-project]:checked');
  const watchingProjects = [];
  $.each(checked, (index, item) => {
    const allIndex = curr_projects.findIndex(proj => proj.web_url === item.value);
    watchingProjects.push(curr_projects[allIndex]);
  });
  currAccount.watching_projects = watchingProjects;
  if (edit_mode === 'add') {
    accounts.push(currAccount);
  } else {
    accounts[currAccountIndex] = currAccount;
  }
  Options.set(STORE_KEY_ACCOUNT, accounts);
  renderAccounts(accounts);

  chrome.extension.sendRequest({type: 'restart'}, function() {
  });
  monitor.log('restart');
  monitor.calc('watching-projects', currAccount.watching_projects.length);

  $('#addAccount').modal('hide');

  return false;
});

function clearAddProject(account, type = 'add') {
  $('#form-watching-project').hide();
  $('#form-add-account').show();
  $('#input-server-url').val(account.serverUrl);
  $('#input-account-name').val(account.name);
  $('#input-aliasname').val(account.aliasName);
  $('#input-username').val(account.userName);
  $('#input-private-token').val(account.privateToken);
  $('#input-notification').val(account.notification);

  $('#help-server-url').parent().removeClass('has-error has-feedback');
  $('#help-server-url').hide();

  if (type === 'add') {
    $('#btn-save').prop('disabled', false);
    $('#btn-nextStep').prop('disabled', true);
    $('#input-server-url').prop('readonly', false);
    $('#help-private-token').attr('href', 'https://gitlab.com/profile/account');
  } else {
    $('#btn-save').prop('disabled', false);
    $('#btn-nextStep').prop('disabled', false);
    $('#input-server-url').prop('readonly', true);
    $('#help-private-token').attr('href', account.serverUrl + '/profile/account');
    getProjects(account.serverUrl, account.privateToken, function(err, project_list) {
      if (err) {
        $('#btn-nextStep').hide();
      } else {
        curr_projects = project_list;
        renderTableView(account.serverUrl, project_list);
        $('#btn-nextStep').prop('disabled', false);
      }
    });
  }
}

function getProjects(serverUrl, privateToken, callback) {
  co(function* () {
    const api = new GitLabAPI({
      server: serverUrl,
      path: '/api/v3',
      token: privateToken,
    });
    const projects = yield api.projects();
    callback(null, projects);
  }).catch(function(err) {
    callback(err);
    console.error(err);
    monitor.error(err);
  });
}

$('#addAccount').on('show.bs.modal', function(evt) {
  const index = parseInt($(evt.relatedTarget).attr('data-index'), 10);
  currAccountIndex = index;
  edit_mode = index >= 0 ? 'edit' : 'add';
  const account = index >= 0 ? accounts[index] : {
    name: '',
    serverUrl: '',
    userName: '',
    aliasName: '',
    privateToken: '',
    notification: 'participating',
  };
  clearAddProject(account, edit_mode);
  if (edit_mode === 'edit') {
    currAccount = account;
  }
});
$('#addAccount').on('shown.bs.modal', function() {
  if (edit_mode === 'add') {
    $('#input-server-url').focus();
  } else {
    $('#input-account-name').focus();
  }
});
$('#addAccount').on('hide.bs.modal', function() {
  currAccount = null;
});

$(document).delegate('[name=options-open-target]', 'click', function() {
  const value = this.value;
  Options.set('open.target', value);
});

i18n.dom();
monitor.pageview();
// stop google analytics when can't connect to google server in China.
// @see http://blog.hotoo.me/post/abort-image-request
window.setTimeout(() => {
  if (document.readyState !== 'complete') {
    window.stop();
  }
}, 1200);
