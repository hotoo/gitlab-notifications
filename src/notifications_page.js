'use strict';

require('./notifications_page.less');

const $ = require('jquery');
const querystring = require('querystring');
const Messages = require('./messages');
const tpl = require('./notifications.atpl');
const i18n = require('./lib/i18n');
const Options = require('./lib/options');
const monitor = require('./lib/monitor');
const util = require('./lib/util');

window.jQuery = $;
require('bootstrap');
require('timeago');

const accounts = Options.get('gitlab.accounts') || [];
const watchingProjects = [];
accounts.forEach(account => {
  watchingProjects.push.apply(watchingProjects, account.watching_projects);
});
const query = querystring.decode(location.search.substring(1));
const currTab = query.tab || 'watching';

function render() {
  let messages = Messages.list({type: 'unread'}).sort((a, b) => {
    const curl = a.project_url.localeCompare(b.project_url);
    if (curl !== 0) return curl;
    return a.timestamp - b.timestamp;
  });
  const participatingMessage = messages.filter(msg => msg.participating);
  if (currTab === 'participating') {
    messages = participatingMessage;
  } else if (currTab === 'watching') {
    // messages = messages.filter(msg => watchingProjects.findIndex(proj => proj.id === msg.project_id) >= 0);
    messages = messages.filter(msg => msg.watching);
  }

  // 按 project_url 进行分组。
  const _msgs = {};
  let last_project_path;
  messages.forEach(msg => {
    const project_url = msg.project_url;
    if (last_project_path !== project_url) {
      _msgs[project_url] = [];
      last_project_path = project_url;
    }
    _msgs[project_url].push(msg);
  });

  const openTarget = Options.get('open.target') || '_self';

  let html = i18n.replace(tpl({
    _msgs,
    currTab,
    openTarget,
    participatingCount: participatingMessage.length,
    util: {
      keys: Object.keys,
      isoDate: function(timestamp) {
        return new Date(timestamp).toISOString();
      },
      safe: util.safe,
    },
  }));
  $('#wrapper').html(html);
  $('[data-toggle="tooltip"]').tooltip({
    delay: {
      show: 500,
      hide: 0,
    },
  });
  $('.timeago').timeago();
}

function bindEvent() {
  $('#btn-mark-all-as-read').click(() => {
    Messages.readAll();
    render();

    chrome.extension.sendRequest({type: 'set-badge'}, function() { });
  });

  $(document).on('click', '.btn-mark-all-as-read-by-repo', function() {
    $(this).parent().parent().parent().children('a.list-group-item').addClass('visited');
    Messages.readRepo($(this).attr('data-repo'));

    chrome.extension.sendRequest({type: 'set-badge'}, function() { });
  });

  // Mark single notification as read.
  $(document).on('click', '.mark-as-read', function(evt) {
    evt.stopPropagation();

    $('[data-toggle="tooltip"]').tooltip('hide');

    const a = $(this).parent().parent('a');
    Messages.read(a.attr('data-key'));
    a.addClass('visited');
    $(this).addClass('visited');

    chrome.extension.sendRequest({type: 'set-badge'}, function() { });

    return false;
  });

  // Mark single notification as mute.
  $(document).on('click', '.mark-as-mute', function(evt) {
    evt.stopPropagation();

    $('[data-toggle="tooltip"]').tooltip('hide');

    const a = $(this).parent().parent('a');
    Messages.mute(a.attr('data-key'));
    a.addClass('visited');
    $(this).addClass('visited');

    chrome.extension.sendRequest({type: 'set-badge'}, function() { });

    return false;
  });
}

render();
bindEvent();
i18n.dom();


monitor.pageview();
// stop google analytics when can't connect to google server in China.
// @see http://blog.hotoo.me/post/abort-image-request
window.setTimeout(() => {
  if (document.readyState !== 'complete') {
    window.stop();
  }
}, 1200);
