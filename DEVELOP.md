# DEVELOP

## projects/:id/events

```
[
{
  "title": null,
  "project_id": 1468,
  "action_name": "joined",
  "target_id": null,
  "target_type": null,
  "author_id": 9359,
  "data": null,
  "target_title": null,
  "created_at": "2016-07-11T15:26:47.000+08:00",
  "author": {
    "name": "兼续",
    "username": "chenglin.mcl",
    "id": 9359,
    "state": "active",
    "avatar_url": "http://024028.oss-cn-hangzhou-zmf.aliyuncs.com/uploads/user/avatar/9359/9359.jpg",
    "web_url": "http://gitlab.alipay-inc.com/u/chenglin.mcl"
  },
  "author_username": "chenglin.mcl"
},



{
  "title": null,
  "project_id": 4937,
  "action_name": "opened",
  "target_id": 65518,
  "target_type": "Issue",
  "author_id": 107,
  "data": null,
  "target_title": "mdap 版 mtracker 字段梳理",
  "created_at": "2016-08-01T09:54:45.000+08:00",
  "author": {
    "name": "祯逸",
    "username": "zhenyi.szy",
    "id": 107,
    "state": "active",
    "avatar_url": "http://024028.oss-cn-hangzhou-zmf.aliyuncs.com/uploads/user/avatar/107/8a82b9014a90f603d1f0cd063912b31bb051ed2b.jpg",
    "web_url": "http://gitlab.alipay-inc.com/u/zhenyi.szy"
  },
  "author_username": "zhenyi.szy"
},
{
  "title": null,
  "project_id": 9988,
  "action_name": "opened",
  "target_id": 59257,
  "target_type": "MergeRequest",
  "author_id": 148,
  "data": null,
  "target_title": "fix: 预发环境下数仓的 appid",
  "created_at": "2016-08-02T21:09:21.000+08:00",
  "author": {
    "name": "天辙",
    "username": "weiesky.wangc",
    "id": 148,
    "state": "active",
    "avatar_url": "http://024028.oss-cn-hangzhou-zmf.aliyuncs.com/uploads/user/avatar/148/128.png",
    "web_url": "http://gitlab.alipay-inc.com/u/weiesky.wangc"
  },
  "author_username": "weiesky.wangc"
},



{
  "title": null,
  "project_id": 306,
  "action_name": "commented on",
  "target_id": 561170,
  "target_type": "Note",
  "author_id": 9373,
  "data": null,
  "target_title": null,
  "created_at": "2016-08-02T21:05:02.000+08:00",
  "note": {
    "id": 561170,
    "body": "2016-08-02\r\n\r\n - securitycenter 静态页面搭建、r\n\r\n 今天主要和师兄商量了一下该怎么搭建 securitycenter 的静态页面，感觉目前主要的积累都已经到了无线端，留在 PC 端的东西不多了。\r\n\r\n 和玉伯老师、小迈姐面谈的还是很开心的，希望拿到有一个好的结果。",
    "attachment": null,
    "author": {
      "name": "暝熹",
      "username": "jiaxun.wjx",
      "id": 9373,
      "state": "active",
      "avatar_url": "http://024028.oss-cn-hangzhou-zmf.aliyuncs.com/uploads/user/avatar/9373/9373.jpg",
      "web_url": "http://gitlab.alipay-inc.com/u/jiaxun.wjx"
    },
    "created_at": "2016-08-02T21:05:01.000+08:00",
    "system": false,
    "noteable_id": 56190,
    "noteable_type": "Issue",
    "upvote": false,
    "downvote": false
  },
  "author": {
    "name": "暝熹",
    "username": "jiaxun.wjx",
    "id": 9373,
    "state": "active",
    "avatar_url": "http://024028.oss-cn-hangzhou-zmf.aliyuncs.com/uploads/user/avatar/9373/9373.jpg",
    "web_url": "http://gitlab.alipay-inc.com/u/jiaxun.wjx"
  },
  "author_username": "jiaxun.wjx"
},
{
  "title": null,
  "project_id": 9988,
  "action_name": "commented on",
  "target_id": 560656,
  "target_type": "Note",
  "author_id": 148,
  "data": null,
  "target_title": null,
  "created_at": "2016-08-02T18:04:03.000+08:00",
  "note": {
    "id": 560656,
    "body": "＋1",
    "attachment": null,
    "author": {
      "name": "天辙",
      "username": "weiesky.wangc",
      "id": 148,
      "state": "active",
      "avatar_url": "http://024028.oss-cn-hangzhou-zmf.aliyuncs.com/uploads/user/avatar/148/128.png",
      "web_url": "http://gitlab.alipay-inc.com/u/weiesky.wangc"
    },
    "created_at": "2016-08-02T18:04:03.000+08:00",
    "system": false,
    "noteable_id": 59160,
    "noteable_type": "MergeRequest",
    "upvote": false,
    "downvote": false
  },
  "author": {
    "name": "天辙",
    "username": "weiesky.wangc",
    "id": 148,
    "state": "active",
    "avatar_url": "http://024028.oss-cn-hangzhou-zmf.aliyuncs.com/uploads/user/avatar/148/128.png",
    "web_url": "http://gitlab.alipay-inc.com/u/weiesky.wangc"
  },
  "author_username": "weiesky.wangc"
},
{
  "title": null,
  "project_id": 9988,
  "action_name": "commented on",
  "target_id": 561218,
  "target_type": "Note",
  "author_id": 1126,
  "data": null,
  "target_title": null,
  "created_at": "2016-08-02T21:33:53.000+08:00",
  "note": {
    "id": 561218,
    "body": "这种到今天才发现？还是他们突然改的",
    "attachment": null,
    "author": {
      "name": "已过",
      "username": "yuguo.zhou",
      "id": 1126,
      "state": "active",
      "avatar_url": "https://work.alibaba-inc.com/photo/64060.240x240xz.jpg",
      "web_url": "http://gitlab.alipay-inc.com/u/yuguo.zhou"
    },
    "created_at": "2016-08-02T21:33:53.000+08:00",
    "system": false,
    "noteable_id": null,
    "noteable_type": "Commit",
    "upvote": false,
    "downvote": false
  },
  "author": {
    "name": "已过",
    "username": "yuguo.zhou",
    "id": 1126,
    "state": "active",
    "avatar_url": "https://work.alibaba-inc.com/photo/64060.240x240xz.jpg",
    "web_url": "http://gitlab.alipay-inc.com/u/yuguo.zhou"
  },
  "author_username": "yuguo.zhou"
},



{
  "title": null,
  "project_id": 9988,
  "action_name": "deleted",
  "target_id": null,
  "target_type": null,
  "author_id": 148,
  "data": {
    "object_kind": "push",
    "before": "2f49d1cf7734b4957a3bd330fea3531126094893",
    "after": "0000000000000000000000000000000000000000",
    "ref": "refs/heads/sit/appid",
    "checkout_sha": null,
    "message": null,
    "user_id": 148,
    "user_name": "天辙",
    "user_email": "weiesky.wangc@alibaba-inc.com",
    "project_id": 9988,
    "repository": {
      "name": "wealthbffweb",
      "url": "git@gitlab.alipay-inc.com:chair_release/wealthbffweb.git",
      "description": "",
      "homepage": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb",
      "git_http_url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb.git",
      "git_ssh_url": "git@gitlab.alipay-inc.com:chair_release/wealthbffweb.git",
      "visibility_level": 10
    },
    "commits": [],
    "total_commits_count": 0
  },
  "target_title": null,
  "created_at": "2016-08-02T21:29:06.000+08:00",
  "author": {
    "name": "天辙",
    "username": "weiesky.wangc",
    "id": 148,
    "state": "active",
    "avatar_url": "http://024028.oss-cn-hangzhou-zmf.aliyuncs.com/uploads/user/avatar/148/128.png",
    "web_url": "http://gitlab.alipay-inc.com/u/weiesky.wangc"
  },
  "author_username": "weiesky.wangc"
},


{
  "title": null,
  "project_id": 9988,
  "action_name": "accepted",
  "target_id": 59257,
  "target_type": "MergeRequest",
  "author_id": 148,
  "data": null,
  "target_title": "fix: 预发环境下数仓的 appid",
  "created_at": "2016-08-02T21:29:02.000+08:00",
  "author": {
    "name": "天辙",
    "username": "weiesky.wangc",
    "id": 148,
    "state": "active",
    "avatar_url": "http://024028.oss-cn-hangzhou-zmf.aliyuncs.com/uploads/user/avatar/148/128.png",
    "web_url": "http://gitlab.alipay-inc.com/u/weiesky.wangc"
  },
  "author_username": "weiesky.wangc"
},



{
  "title": null,
  "project_id": 9988,
  "action_name": "pushed new",
  "target_id": null,
  "target_type": null,
  "author_id": 148,
  "data": {
    "object_kind": "push",
    "before": "0000000000000000000000000000000000000000",
    "after": "2f49d1cf7734b4957a3bd330fea3531126094893",
    "ref": "refs/heads/sit/appid",
    "checkout_sha": "2f49d1cf7734b4957a3bd330fea3531126094893",
    "message": null,
    "user_id": 148,
    "user_name": "天辙",
    "user_email": "weiesky.wangc@alibaba-inc.com",
    "project_id": 9988,
    "repository": {
      "name": "wealthbffweb",
      "url": "git@gitlab.alipay-inc.com:chair_release/wealthbffweb.git",
      "description": "",
      "homepage": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb",
      "git_http_url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb.git",
      "git_ssh_url": "git@gitlab.alipay-inc.com:chair_release/wealthbffweb.git",
      "visibility_level": 10
    },
    "commits": [{
      "id": "2f49d1cf7734b4957a3bd330fea3531126094893",
      "message": "fix: 预发环境下数仓的 appid\n",
      "timestamp": "2016-08-02T21:07:22+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/2f49d1cf7734b4957a3bd330fea3531126094893",
      "author": {
        "name": "天辙",
        "email": "weiesky.wangc@alibaba-inc.com"
      }
    }],
    "total_commits_count": 1
  },
  "target_title": null,
  "created_at": "2016-08-02T21:07:30.000+08:00",
  "author": {
    "name": "天辙",
    "username": "weiesky.wangc",
    "id": 148,
    "state": "active",
    "avatar_url": "http://024028.oss-cn-hangzhou-zmf.aliyuncs.com/uploads/user/avatar/148/128.png",
    "web_url": "http://gitlab.alipay-inc.com/u/weiesky.wangc"
  },
  "author_username": "weiesky.wangc"
},
{
  "title": null,
  "project_id": 9988,
  "action_name": "pushed new",
  "target_id": null,
  "target_type": null,
  "author_id": 2122,
  "data": {
    "object_kind": "push",
    "before": "0000000000000000000000000000000000000000",
    "after": "dfea4e0b09dc7bca5d5f45192d62afd7edb41a48",
    "ref": "refs/heads/fix/product_detail",
    "checkout_sha": "dfea4e0b09dc7bca5d5f45192d62afd7edb41a48",
    "message": null,
    "user_id": 2122,
    "user_name": "狨猴",
    "user_email": "wc99871@alipay.com",
    "project_id": 9988,
    "repository": {
      "name": "wealthbffweb",
      "url": "git@gitlab.alipay-inc.com:chair_release/wealthbffweb.git",
      "description": "",
      "homepage": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb",
      "git_http_url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb.git",
      "git_ssh_url": "git@gitlab.alipay-inc.com:chair_release/wealthbffweb.git",
      "visibility_level": 10
    },
    "commits": [{
      "id": "c910820af3f849a009e8d87d1b98aaa9e298f5bb",
      "message": "fix:delete mock\n",
      "timestamp": "2016-07-29T17:07:12+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/c910820af3f849a009e8d87d1b98aaa9e298f5bb",
      "author": {
        "name": "nathan.wn",
        "email": "nathan.wn@alibaba-inc.com"
      }
    }, {
      "id": "61349d7618a41e6c8b2885ece8d5a7838ddef5f0",
      "message": "Merge branch 'feat_redord' into 'ANT01118569_normal_20160624_wealthbffweb'\r\n\r\nfix: 修复如果时间为 null，getTime 报错的问题、r\n\r\n\r\n\r\nSee merge request !430",
      "timestamp": "2016-07-29T17:14:17+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/61349d7618a41e6c8b2885ece8d5a7838ddef5f0",
      "author": {
        "name": "余化",
        "email": "shuai.shao@alipay.com"
      }
    }, {
      "id": "9899e921e22279c605bd5481a1173b4d691d968e",
      "message": "fix: 累计加速收益率为 0 时，实际输出为 0\n",
      "timestamp": "2016-08-01T11:02:27+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/9899e921e22279c605bd5481a1173b4d691d968e",
      "author": {
        "name": "ronghou",
        "email": "wc99871@alipay.com"
      }
    }, {
      "id": "d9271bc61fa645cfda829d9a8610ead1d1835f0a",
      "message": "fix: 资产首页 - 后端修改字段：instTransaction 代替 transactionEndConfirmDate\n",
      "timestamp": "2016-08-01T11:38:08+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/d9271bc61fa645cfda829d9a8610ead1d1835f0a",
      "author": {
        "name": "余化",
        "email": "shuai.shao@alipay.com"
      }
    }, {
      "id": "cfc5d1c8ddbf8b7fe51283c0cc7a667472693007",
      "message": "Merge branch 'fix-assets-index-confirm-date-field-change' into 'ANT01118569_normal_20160624_wealthbffweb'\r\n\r\nfix: 资产首页 - 后端修改字段：instTransaction 代替 transactionEndConfirmDate\r\n\r\n\r\n\r\nSee merge request !436",
      "timestamp": "2016-08-01T11:40:17+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/cfc5d1c8ddbf8b7fe51283c0cc7a667472693007",
      "author": {
        "name": "狨猴",
        "email": "wc99871@alipay.com"
      }
    }, {
      "id": "16fd3ff6658e757b31232e0119b74e2b7dfb56d3",
      "message": "Merge branch 'fix/asset_detail' into 'ANT01118569_normal_20160624_wealthbffweb'\r\n\r\nfix: 累计加速收益率为 0 时，实际输出为 0\r\n\r\n\r\n\r\nSee merge request !437",
      "timestamp": "2016-08-01T12:05:46+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/16fd3ff6658e757b31232e0119b74e2b7dfb56d3",
      "author": {
        "name": "已过",
        "email": "yuguo.zhou@alibaba-inc.com"
      }
    }, {
      "id": "8ab4d1aeffa743aa59de93af1884277d0678f368",
      "message": "fix: 修改 fix2tail 函数，处理 2.9999 类情况、n",
      "timestamp": "2016-08-01T16:38:09+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/8ab4d1aeffa743aa59de93af1884277d0678f368",
      "author": {
        "name": "已过",
        "email": "yuguo.zhou@alibaba-inc.com"
      }
    }, {
      "id": "cb7518a0bebbab7bbf72cf180fe7fafcc0677bb3",
      "message": "Merge branch 'yiguo-asset-detail' into 'ANT01118569_normal_20160624_wealthbffweb'\r\n\r\nfix: 修改 fix2tail 函数，处理 2.9999 类情况、r\n\r\nfix: 修改 fix2tail 函数，处理 2.9999 类情况、r\n\r\nSee merge request !439",
      "timestamp": "2016-08-01T17:28:10+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/cb7518a0bebbab7bbf72cf180fe7fafcc0677bb3",
      "author": {
        "name": "已过",
        "email": "yuguo.zhou@alibaba-inc.com"
      }
    }, {
      "id": "3675d82711af0d2418e37800e3638dce7c68dfa3",
      "message": "fix: fix fund test\n",
      "timestamp": "2016-08-01T19:12:13+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/3675d82711af0d2418e37800e3638dce7c68dfa3",
      "author": {
        "name": "余化",
        "email": "shuai.shao@alipay.com"
      }
    }, {
      "id": "cc26883b1fe1a338e9a628c23eb592b32fb2e542",
      "message": "Merge branch 'fix-fund-tests' into 'ANT01118569_normal_20160624_wealthbffweb'\r\n\r\nfix: fix fund test\r\n\r\n\r\n\r\nSee merge request !442",
      "timestamp": "2016-08-01T19:13:47+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/cc26883b1fe1a338e9a628c23eb592b32fb2e542",
      "author": {
        "name": "狨猴",
        "email": "wc99871@alipay.com"
      }
    }, {
      "id": "4e1c70580b16ffcc35d05df1bcffec45916c46b9",
      "message": "fix: 增加产品详情错误日志的监控、n",
      "timestamp": "2016-08-02T05:01:54+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/4e1c70580b16ffcc35d05df1bcffec45916c46b9",
      "author": {
        "name": "ronghou",
        "email": "wc99871@alipay.com"
      }
    }, {
      "id": "707ff9a39e2bf47420ddd3d005a3b85312118fe0",
      "message": "Merge branch 'fix/fix_error' into 'ANT01118569_normal_20160624_wealthbffweb'\r\n\r\nfix: 增加产品详情错误日志的监控、r\n\r\n\r\n\r\nSee merge request !448",
      "timestamp": "2016-08-02T10:51:15+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/707ff9a39e2bf47420ddd3d005a3b85312118fe0",
      "author": {
        "name": "余化",
        "email": "shuai.shao@alipay.com"
      }
    }, {
      "id": "b8ef8acae969b467cc9972eb7fd6cd0969e5cde1",
      "message": "fix: 处理 0.9999 类情况、u0026\u0026 拆分单测、n",
      "timestamp": "2016-08-02T14:29:01+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/b8ef8acae969b467cc9972eb7fd6cd0969e5cde1",
      "author": {
        "name": "nathan.wn",
        "email": "nathan.wn@alibaba-inc.com"
      }
    }, {
      "id": "d8fe90080ee489d52550f89571fb57416945d3b1",
      "message": "fix: 处理 undefined 情况、n",
      "timestamp": "2016-08-02T14:59:36+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/d8fe90080ee489d52550f89571fb57416945d3b1",
      "author": {
        "name": "nathan.wn",
        "email": "nathan.wn@alibaba-inc.com"
      }
    }, {
      "id": "f70cff3fd6372a651509712feebf4d75822c762d",
      "message": "Merge branch 'feat_redord' into 'ANT01118569_normal_20160624_wealthbffweb'\r\n\r\nfix: 处理 0.9999 类情况、r\n\r\n\r\n\r\nSee merge request !440",
      "timestamp": "2016-08-02T15:04:43+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/f70cff3fd6372a651509712feebf4d75822c762d",
      "author": {
        "name": "狨猴",
        "email": "wc99871@alipay.com"
      }
    }, {
      "id": "fba41ecad058b3faa6f9faae93415e73d1029e54",
      "message": "fix: 乐买宝 - 修复资产首页接口，当收益增速非常小时（yesterdayRebateRate=0）显示、u003c0.01\n",
      "timestamp": "2016-08-02T15:52:27+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/fba41ecad058b3faa6f9faae93415e73d1029e54",
      "author": {
        "name": "余化",
        "email": "shuai.shao@alipay.com"
      }
    }, {
      "id": "ccb337bfa3c4bc873c97db73c7dd4da8d6aff873",
      "message": "Merge branch 'fix-assets-index-yesterdayRebateRate' into 'ANT01118569_normal_20160624_wealthbffweb'\r\n\r\nfix: 乐买宝 - 修复资产首页接口，当收益增速非常小时（yesterdayRebateRate=0）显示、u003c0.01\r\n\r\n\r\n\r\nSee merge request !451",
      "timestamp": "2016-08-02T15:59:36+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/ccb337bfa3c4bc873c97db73c7dd4da8d6aff873",
      "author": {
        "name": "狨猴",
        "email": "wc99871@alipay.com"
      }
    }, {
      "id": "d52f8131411abf8b0a8ae49a228e6aa9bdd2217e",
      "message": "feat: 乐买宝 - 资产首页 - 增加打印错误日志到 bizLogger\n",
      "timestamp": "2016-08-02T17:02:17+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/d52f8131411abf8b0a8ae49a228e6aa9bdd2217e",
      "author": {
        "name": "余化",
        "email": "shuai.shao@alipay.com"
      }
    }, {
      "id": "1b69433504e8a6f154116a61b9756def10106c57",
      "message": "Merge branch 'feat_assets_index_biz_log' into 'ANT01118569_normal_20160624_wealthbffweb'\r\n\r\nfeat: 乐买宝 - 资产首页 - 增加打印错误日志到 bizLogger\r\n\r\n\r\n\r\nSee merge request !452",
      "timestamp": "2016-08-02T17:06:12+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/1b69433504e8a6f154116a61b9756def10106c57",
      "author": {
        "name": "狨猴",
        "email": "wc99871@alipay.com"
      }
    }, {
      "id": "dfea4e0b09dc7bca5d5f45192d62afd7edb41a48",
      "message": "fix: 服务端返回的收益率乘以了 100\n",
      "timestamp": "2016-08-02T20:59:22+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/dfea4e0b09dc7bca5d5f45192d62afd7edb41a48",
      "author": {
        "name": "ronghou",
        "email": "wc99871@alipay.com"
      }
    }],
    "total_commits_count": 128
  },
  "target_title": null,
  "created_at": "2016-08-02T20:59:47.000+08:00",
  "author": {
    "name": "狨猴",
    "username": "wc99871",
    "id": 2122,
    "state": "active",
    "avatar_url": "https://work.alibaba-inc.com/photo/99871.240x240xz.jpg",
    "web_url": "http://gitlab.alipay-inc.com/u/wc99871"
  },
  "author_username": "wc99871"
},




{
  "title": null,
  "project_id": 9988,
  "action_name": "pushed to",
  "target_id": null,
  "target_type": null,
  "author_id": 148,
  "data": {
    "object_kind": "push",
    "before": "7a01652ea9572786d6c7b1a65ac638fc1fe02527",
    "after": "16e38dafc65f43278b5bd9e9ab80a709b9a44436",
    "ref": "refs/heads/master",
    "checkout_sha": "16e38dafc65f43278b5bd9e9ab80a709b9a44436",
    "message": null,
    "user_id": 148,
    "user_name": "天辙",
    "user_email": "weiesky.wangc@alibaba-inc.com",
    "project_id": 9988,
    "repository": {
      "name": "wealthbffweb",
      "url": "git@gitlab.alipay-inc.com:chair_release/wealthbffweb.git",
      "description": "",
      "homepage": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb",
      "git_http_url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb.git",
      "git_ssh_url": "git@gitlab.alipay-inc.com:chair_release/wealthbffweb.git",
      "visibility_level": 10
    },
    "commits": [{
      "id": "2f49d1cf7734b4957a3bd330fea3531126094893",
      "message": "fix: 预发环境下数仓的 appid\n",
      "timestamp": "2016-08-02T21:07:22+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/2f49d1cf7734b4957a3bd330fea3531126094893",
      "author": {
        "name": "天辙",
        "email": "weiesky.wangc@alibaba-inc.com"
      }
    }, {
      "id": "16e38dafc65f43278b5bd9e9ab80a709b9a44436",
      "message": "Merge branch 'sit/appid' into 'master'\r\n\r\nfix: 预发环境下数仓的 appid\r\n\r\n@mingzi.xmz @hui.jiao \r\n\r\nSee merge request !455",
      "timestamp": "2016-08-02T21:28:58+08:00",
      "url": "http://gitlab.alipay-inc.com/chair_release/wealthbffweb/commit/16e38dafc65f43278b5bd9e9ab80a709b9a44436",
      "author": {
        "name": "天辙",
        "email": "weiesky.wangc@alibaba-inc.com"
      }
    }],
    "total_commits_count": 2
  },
  "target_title": null,
  "created_at": "2016-08-02T21:29:09.000+08:00",
  "author": {
    "name": "天辙",
    "username": "weiesky.wangc",
    "id": 148,
    "state": "active",
    "avatar_url": "http://024028.oss-cn-hangzhou-zmf.aliyuncs.com/uploads/user/avatar/148/128.png",
    "web_url": "http://gitlab.alipay-inc.com/u/weiesky.wangc"
  },
  "author_username": "weiesky.wangc"
},



{
  "title": null,
  "project_id": 4937,
  "action_name": "closed",
  "target_id": 65062,
  "target_type": "Issue",
  "author_id": 48,
  "data": null,
  "target_title": "亲情账号认证流程快捷引导页增加协议链接",
  "created_at": "2016-08-02T11:51:34.000+08:00",
  "author": {
    "name": "宗渊",
    "username": "xingyun.wxy",
    "id": 48,
    "state": "active",
    "avatar_url": "http://024028.oss-cn-hangzhou-zmf.aliyuncs.com/uploads/user/avatar/48/cat.jpg",
    "web_url": "http://gitlab.alipay-inc.com/u/xingyun.wxy"
  },
  "author_username": "xingyun.wxy"
},
{
  "title": null,
  "project_id": 4937,
  "action_name": "closed",
  "target_id": 5629,
  "target_type": "Milestone",
  "author_id": 48,
  "data": null,
  "target_title": "『其他』 custweb 亲情账号紧急发布",
  "created_at": "2016-08-02T11:51:42.000+08:00",
  "author": {
    "name": "宗渊",
    "username": "xingyun.wxy",
    "id": 48,
    "state": "active",
    "avatar_url": "http://024028.oss-cn-hangzhou-zmf.aliyuncs.com/uploads/user/avatar/48/cat.jpg",
    "web_url": "http://gitlab.alipay-inc.com/u/xingyun.wxy"
  },
  "author_username": "xingyun.wxy"
},
]
```

## 异步通知消息

根据 key 去重，如果 key 已经存在，则保留大部分的字段不覆盖，
但是如果已存在的消息是未参与的，新消息变为已参与状态，则更改 participation 字段。

Open new issue:

```json
{
  key: 'http://gitlab.company.com/:group/:repo/issues/:issueId',
  title: 'issue title',
  author_id: 'author_id',
  author_name: 'author_name',
  body: 'issue content',
  url: 'http://gitlab.company.com/:group/:repo/issues/:issueId',
  participating: false,
  project_path: 'group/repo',
  project_url: 'http://gitlab.company.com/group/repo',
}
```

Comment on issue:

```json
{
  key: 'http://gitlab.company.com/:group/:repo/issues/:issueId',
  title: 'issue title',
  author_id: 'author_id',
  author_name: 'author_name',
  body: 'issue content',
  url: 'http://gitlab.company.com/:group/:repo/issues/:issueId#:note_id',
  participating: false,
  project_name: 'group/repo',
  project_url: 'http://gitlab.company.com/group/repo',
}
```



## 弹窗通知消息

Open new issue:

```json
{
  key: 'http://gitlab.company.com/repo/project#issueId',
  title: '@author opened a new issue',
  body: 'issue title',
  url: 'http://gitlab.company.com/repo/project/issueId'
}
```

Comment on issue:

```json
{
  key: 'http://gitlab.company.com/repo/project#issueId',
  title: '@author: #issueId - issue title',
  body: 'comment content.',
  url: 'http://gitlab.company.com/repo/project/issueId#note_id'
}
```

Close a issue:

```json
{
  key: 'http://gitlab.company.com/repo/project#issueId',
  title: '@author close issue #issueId',
  body: 'issue title',
  url: 'http://gitlab.company.com/repo/project/issueId#note_id'
}
```

Reopen a issue:

```json
{
  key: 'http://gitlab.company.com/repo/project#issueId',
  title: '@author reopen issue #issueId',
  body: 'issue title',
  url: 'http://gitlab.company.com/repo/project/issueId#note_id'
}
```

## 全局动态消息 (activity)

### mergerequest-opened

```html
<div class="event-block event-item">
  <div class="event-item-timestamp">
    <time class="time_ago js-timeago js-timeago-pending" datetime="2016-08-16T08:42:11Z"
      title="Aug 16, 2016 4:42pm" data-toggle="tooltip" data-placement="top"
      data-container="body">2016-08-16 16:42:11 +0800
    </time>
    <script>
    //<![CDATA[
    $('.js-timeago-pending').removeClass('js-timeago-pending').timeago()
    //]]>
    </script>
  </div>
  <img class="avatar s46" alt="" src="http://024028.oss-cn-hangzhou-zmf.aliyuncs.com/uploads/user/avatar/308/308.jpg">
  <div class="event-title">
    <span class="author_name"><a href="/u/daqiu.lym">大遒</a></span>
    <span class="event_label opened">
    opened merge request
    </span>
    <strong><a href="/chair_release/finsignweb/merge_requests/341">#341</a></strong>
    at
    <a href="/chair_release/finsignweb"><span class="namespace-name">chair_release / </span><span class="project-name">finsignweb</span></a>
  </div>
  <div class="event-body">
    <div class="event-note">
    增加开关  &amp; lint
    </div>
  </div>
</div>
```

### mergerequest-accepted

```html
<div class="event-block event-item">
  <div class="event-item-timestamp">
    <time class="time_ago js-timeago js-timeago-pending" datetime="2016-08-16T08:11:42Z"
      title="Aug 16, 2016 4:11pm" data-toggle="tooltip" data-placement="top"
      data-container="body">2016-08-16 16:11:42 +0800
    </time>
    <script>
    //<![CDATA[
    $('.js-timeago-pending').removeClass('js-timeago-pending').timeago()
    //]]>
    </script>
  </div>
  <img class="avatar s46" alt="" src="https://work.alibaba-inc.com/photo/87818.240x240xz.jpg">
  <div class="event-title">
    <span class="author_name">
      <a href="/u/lanxi.slx">兰惜</a>
    </span>
    <span class="accepted event_label">
      accepted merge request
    </span>
    <strong>
      <a href="/AlipayWealth/afwealth_ios/merge_requests/1704">#1704</a>
    </strong>
    at
    <a href="/AlipayWealth/afwealth_ios">
      <span class="namespace-name">AlipayWealth / </span>
      <span class="project-name">AFWealth_iOS</span>
    </a>
  </div>
  <div class="event-body">
    <div class="event-note">
    D:『富文本链接色值可配置』
    </div>
  </div>
</div>
```

### mergerequest-closed

```html
<div class="event-block event-item">
<div class="event-item-timestamp">
<time class="time_ago js-timeago js-timeago-pending" datetime="2016-08-23T09:13:33Z" title="Aug 23, 2016 5:13pm" data-toggle="tooltip" data-placement="top" data-container="body">2016-08-23 17:13:33 +0800</time><script>
//<![CDATA[
$('.js-timeago-pending').removeClass('js-timeago-pending').timeago()
//]]>
</script>
</div>
<img class="avatar s46" alt="" src="http://024028.oss-cn-hangzhou-zmf.aliyuncs.com/uploads/user/avatar/90/______A_.jpg">
<div class="event-title">
<span class="author_name"><a href="/u/huachang.chc">夜末 (yè mò)</a></span>
<span class="closed event_label">
closed merge request
</span>
<strong><a href="/chair/egg-mbp/merge_requests/59">#59</a></strong>
at
<a href="/chair/egg-mbp"><span class="namespace-name">chair / </span><span class="project-name">egg-mbp</span></a>
</div>
<div class="event-body">
<div class="event-note">
feat(ua): channelInfo 里面透出 ua 信息
</div>
</div>

</div>
```



```html
<div class="event-block event-item">
<div class="event-item-timestamp">
<time class="time_ago js-timeago js-timeago-pending" datetime="2016-08-23T09:15:36Z" title="Aug 23, 2016 5:15pm" data-toggle="tooltip" data-placement="top" data-container="body">2016-08-23 17:15:36 +0800</time><script>
//<![CDATA[
$('.js-timeago-pending').removeClass('js-timeago-pending').timeago()
//]]>
</script>
</div>
<img class="avatar s46" alt="" src="http://024028.oss-cn-hangzhou-zmf.aliyuncs.com/uploads/user/avatar/90/______A_.jpg">
<div class="event-title">
<span class="author_name"><a href="/u/huachang.chc">夜末 (yè mò)</a></span>
<span class="closed event_label">
closed merge request
</span>
<strong><a href="/chair_release/wealthbffweb/merge_requests/587">#587</a></strong>
at
<a href="/chair_release/wealthbffweb"><span class="namespace-name">chair_release / </span><span class="project-name">wealthbffweb</span></a>
</div>
<div class="event-body">
<div class="event-note">
fix(fix_sign): 修复定投教育在聚宝中取不到 UA
</div>
</div>

</div>
```

### issue-opened

```html
<div class="event-block event-item">
  <div class="event-item-timestamp">
    <time class="time_ago js-timeago js-timeago-pending" datetime="2016-08-16T08:45:26Z"
      title="Aug 16, 2016 4:45pm" data-toggle="tooltip" data-placement="top"
      data-container="body">2016-08-16 16:45:26 +0800
    </time>
    <script>
    //<![CDATA[
    $('.js-timeago-pending').removeClass('js-timeago-pending').timeago()
    //]]>
    </script>
  </div>
  <img class="avatar s46" alt="" src="https://work.alibaba-inc.com/photo/81333.240x240xz.jpg">
  <div class="event-title">
    <span class="author_name"><a href="/u/maping.mp">怀音</a></span>
    <span class="event_label opened">
      opened issue
    </span>
    <strong><a href="/antp/qa/issues/380">#380</a></strong>
    at
    <a href="/antp/qa"><span class="namespace-name">antp / </span><span class="project-name">qa</span></a>
  </div>
  <div class="event-body">
    <div class="event-note">
      Steps 如何设置每个 step 的最大宽度？
    </div>
  </div>
</div>
```

### issue-closed

```html
<div class="event-block event-item">
  <div class="event-item-timestamp">
    <time class="time_ago js-timeago js-timeago-pending" datetime="2016-08-16T08:59:19Z"
      title="Aug 16, 2016 4:59pm" data-toggle="tooltip" data-placement="top"
      data-container="body">2016-08-16 16:59:19 +0800
    </time>
    <script>
    //<![CDATA[
    $('.js-timeago-pending').removeClass('js-timeago-pending').timeago()
    //]]>
    </script>
  </div>
  <img class="avatar s46" alt="" src="http://024028.oss-cn-hangzhou-zmf.aliyuncs.com/uploads/user/avatar/11/gitlab.jpg">
  <div class="event-title">
    <span class="author_name"><a href="/u/suqian.yf">苏千</a></span>
    <span class="closed event_label">
      closed issue
    </span>
    <strong><a href="/chair/chair/issues/1929">#1929</a></strong>
    at
    <a href="/chair/chair"><span class="namespace-name">chair / </span><span class="project-name">chair</span></a>
  </div>
  <div class="event-body">
    <div class="event-note">
    本地 run dev 报错
    </div>
  </div>
</div>
```
